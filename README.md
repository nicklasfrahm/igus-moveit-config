# Igus MoveIt Config

A MoveIt configuration for the Igus Robolink 5DOF.

## Development

To adjust the package, run `roslaunch moveit_setup_assistant setup_assistant.launch`.
To run the demo, run `roslaunch igus_moveit_config demo.launch`.

## License

This project is licensed under the terms of the [MIT license](./LICENSE.md).
